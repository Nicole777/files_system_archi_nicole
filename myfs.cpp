#include "myfs.h"
#include <string.h>
#include <iostream>
#include <math.h>
#include <sstream>
#include <fstream>
// defined calc. values for amount
#define CALC_VALUE_10_PERC_OF_BLOCKS_FOR_INODES 0.1

// defined FS values
#define MAGIC_NUMBER 0xf0f03410
#define INODES_PER_BLOCK 128
#define POINTERS_PER_INODE 5
#define POINTERS_PER_BLOCK 1024
#define BLOCK_SIZE 1024
#define INODE_BLOCKS_NUM 2000//random number
const char *MyFs::MYFS_MAGIC = "MYFS";

// tried use structs from https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project06.html

// Superblock structure
struct SuperBlock 
{   
    int MagicNumber;   // File system magic number
    int Blocks;        // Number of blocks in file system
    int InodeBlocks;   // Number of blocks reserved for inodes
    int Inodes;        // Number of inodes in file system
};


// Inode structure
struct Inode 
{              
    int Valid;         // Whether or not inode is valid
    int Size;          // Size of file
    int Direct[POINTERS_PER_INODE]; // Direct pointers
    int Indirect;      // Indirect pointer
};

// block advanced structure - union
union Block 
{
    SuperBlock  Super;                          // Superblock
    Inode       Inodes[INODES_PER_BLOCK];       // Inode block
    int    Pointers[POINTERS_PER_BLOCK];   // Pointer block
    char        Data[BLOCK_SIZE];         // Data block - SIZE wiil be acc to the block size(?)
};


MyFs::MyFs(BlockDeviceSimulator *blkdevsim_):blkdevsim(blkdevsim_) 
{
	struct myfs_header header;
	blkdevsim->read(0, sizeof(header), (char *)&header);

	if (strncmp(header.magic, MYFS_MAGIC, sizeof(header.magic)) != 0 ||
	    (header.version != CURR_VERSION)) 
		{
		std::cout << "Did not find myfs instance on blkdev" << std::endl;
		std::cout << "Creating..." << std::endl;
		format();
		std::cout << "Finished!" << std::endl;
	}
}


void MyFs::format() 
{
	// put the header in place
	struct myfs_header header;
	strncpy(header.magic, MYFS_MAGIC, sizeof(header.magic));
	header.version = CURR_VERSION;
	blkdevsim->write(0, sizeof(header), (const char*)&header);

	// TODO: put your format code here
	Block block; // write a block and its paramethers
	block.Super.MagicNumber = MAGIC_NUMBER;
	block.Super.Blocks = BLOCK_SIZE;

	block.Super.InodeBlocks = int(BLOCK_SIZE * CALC_VALUE_10_PERC_OF_BLOCKS_FOR_INODES);
	block.Super.Inodes = INODES_PER_BLOCK * block.Super.InodeBlocks;
}

//adding empty inode in inode table
void MyFs::create_file(std::string path_str, bool directory) 
{
	int curr_inode = -1;
	for (int inode_block = 0; inode_block < INODE_BLOCKS_NUM; inode_block++)
	{
		Block b;
	// reads each inode
		for (int inode = 0; inode < INODES_PER_BLOCK; inode++) 
		{
			if (!b.Inodes[inode].Valid) // write if no vaildacity
			{
				curr_inode = inode + INODES_PER_BLOCK * inode_block;
				return;
			}
		}
	}
	Inode currInd; // a new node
	currInd.Valid = true;
	currInd.Size = 0;
	for (int i = 0; i < POINTERS_PER_INODE; i++) 
	{
		currInd.Direct[i] = directory;
	}

}


// function for reading the content of a file
std::string MyFs::get_content(std::string path_str) {
	std::ofstream curr_file;
	std::string content_str;
	
	curr_file.open(path_str);
	curr_file << content_str; //  file's content into string
	return content_str;
}

// function for writing the content of a file 
void MyFs::set_content(std::string path_str, std::string content) {
	std::ofstream curr_file;

	curr_file.open(path_str);
    curr_file << content;
    curr_file.close();
}



